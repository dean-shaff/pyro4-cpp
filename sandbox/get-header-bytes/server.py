import sys

sys.path.insert(0, "./Pyro4/src")

import Pyro4
from Pyro4 import config


@Pyro4.expose
class Server:

    def method(self):
        pass


if __name__ == "__main__":
    with Pyro4.Daemon("localhost", 9091) as daemon:
        server = Server()
        uri = daemon.register(server, objectId="Server")
        print(uri)
        daemon.requestLoop()
