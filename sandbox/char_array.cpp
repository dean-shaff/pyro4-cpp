#include <string>
#include <cstring>
#include <iostream>
#include <vector>


#include "test/util.hpp"

const std::string file_path = "/home/SWIN/dshaff/personal/pyro4-cpp/test/MSG_CONNECT.1.pickle.1.bytes.dat";

int main () {

  std::vector<char> bytes;

  test::util::load_binary_data(file_path, bytes);

  char tag[4];
  std::cerr << "tag=" << tag << std::endl;
  std::string tag_str_expected = "PYRO";

  size_t tag_size = sizeof(tag) / sizeof(tag[0]);

  std::memcpy((void*) &tag, (void*) bytes.data(), tag_size);

  for (unsigned idx=0; idx<tag_size; idx++) {
    std::cerr << tag[idx] << "," << bytes[idx] << std::endl;
  }

  std::string tag_str (tag, tag_size);

  std::cerr << "tag_size=" << tag_size << std::endl;
  std::cerr << "tag=" << tag << std::endl;
  std::cerr << (tag_str == tag_str_expected) << std::endl;
}
