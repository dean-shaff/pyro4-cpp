#include <iostream>


void modify_val (int& val) {
  val = 6;
}

void net_to_host () {
  std::cerr << "net_to_host()" << std::endl;
}

template<typename T>
void net_to_host (T arg) {
  std::cerr << "net_to_host(T)" << std::endl;
}

template<typename T, typename... Types>
void net_to_host (T arg, Types... Fargs) {
  std::cerr << "net_to_host(T, Types...)" << std::endl;
  net_to_host(arg);
  net_to_host(Fargs...);
}

int main () {
  int val = 10;
  std::cerr << "val (before) " << val << std::endl;
  modify_val (val);
  std::cerr << "val (after) " << val << std::endl;

  net_to_host(9, 5, 6);

}
