import asyncio


async def handle_client(reader, writer):
    print("got a connection!")
    msg = await reader.readuntil()
    print(msg)

    writer.write(b"hello")
    await writer.drain()
    writer.close()

loop = asyncio.get_event_loop()
loop.create_task(asyncio.start_server(handle_client, 'localhost', 8888))
loop.run_forever()
