#ifndef __test_util_hpp
#define __test_util_hpp

#include <vector>
#include <fstream>


namespace test {
  namespace util {
    template<typename T>
    void load_binary_data (
      const std::string& file_path,
      std::vector<T>& data
    )
    {
      std::streampos size;

      std::ifstream file (file_path, std::ios::in|std::ios::binary|std::ios::ate);
      if (file.is_open())
      {
        file.seekg(0, std::ios::end);
        size = file.tellg();
        file.seekg(0, std::ios::beg);

        // read the data:
        std::vector<char> file_bytes(size);
        file.read(&file_bytes[0], size);

        unsigned T_size = (size / sizeof(T));

        const T* data_ptr = reinterpret_cast<const T*>(file_bytes.data());
        data.assign(data_ptr, data_ptr + T_size);
      }
      file.close();
    }
  }
}

#endif
