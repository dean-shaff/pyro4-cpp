set(test_sources
  test_protocol.cpp
  test_main.cpp
)

set(test_sources
  ${test_sources}
  ${CMAKE_SOURCE_DIR}/src/pyro4/protocol.cpp
  ${CMAKE_SOURCE_DIR}/src/pyro4/constants.cpp
)

add_executable(test_main
  ${test_sources}
)

target_include_directories(test_main
  PUBLIC
  ${CMAKE_SOURCE_DIR}/src
)
