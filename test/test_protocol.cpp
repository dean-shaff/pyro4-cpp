#include <string>
#include <map>
#include <tuple>
#include <iostream>

#include "catch.hpp"
#include "pyro4/protocol.h"

#include "util.hpp"

const std::map<std::string, std::tuple<int, int>> header_file_names = {
  {"MSG_CONNECT.1.pickle.1.bytes.dat", {1, 1}},
  {"MSG_CONNECTOK.2.pickle.1.bytes.dat", {2, 1}},
  {"MSG_INVOKE.4.pickle.1.bytes.dat", {4, 1}},
  {"MSG_RESULT.5.pickle.1.bytes.dat", {5, 1}}
};

const std::string current_file = __FILE__;
const std::size_t found = current_file.find_last_of("/\\");
const std::string current_dir = current_file.substr(0, found + 1);


class BufferStream {

  public:

    BufferStream () { }

    BufferStream (std::vector<char>& _buffer): buffer(_buffer)
    {
      buffer_it = buffer.begin();
    }

    BufferStream& operator= (const BufferStream& _stream) {
      if (this == &_stream) {
        return *this;
      }
      buffer = _stream.buffer;
      buffer_it = buffer.begin();
      return *this;
    }

    void read(char* bytes, size_t size) const {
      std::vector<char>::iterator end_it = buffer_it + size;
      char* bytes_ptr = bytes;
      while (buffer_it != end_it) {
        *bytes_ptr = *buffer_it;
        bytes_ptr++;
        buffer_it++;
      }
    }

    void write(const char* bytes, size_t size) {
      std::vector<char>::iterator end_it = buffer_it + size;
      const char* bytes_ptr = bytes;
      while (buffer_it != end_it) {
        *buffer_it = *bytes_ptr;
        bytes_ptr++;
        buffer_it++;
      }
    }

  protected:

    std::vector<char> buffer;
    mutable std::vector<char>::iterator buffer_it;

};



TEST_CASE (
  "Protocol Message header is correct",
  "[protocol][Header]"
)
{
  pyro4::protocol::Message::Header header;
  REQUIRE(sizeof(header) == 24);

  SECTION ("deserialize turns bytes into header object, and serialize goes back to bytes")
  {
    // std::cerr << "current_dir=" << current_dir << std::endl;
    std::string file_path;
    int msg_type;
    int ser_id;

    std::vector<char> bytes;
    std::vector<char> test_bytes(sizeof(header));

    for (auto const& pair : header_file_names) {
      file_path = current_dir + pair.first;
      msg_type = std::get<0>(pair.second);
      ser_id = std::get<1>(pair.second);

      test::util::load_binary_data<char>(file_path, bytes);
      // now get the first header_size bytes
      bytes = std::vector<char>(bytes.begin(), bytes.begin() + sizeof(header));
      // deserialize the header
      pyro4::protocol::Message::Header::deserialize(bytes, header);
      CHECK(header.msg_type == msg_type);
      CHECK(header.serializer_id == ser_id);
      // serialize the data back into bytes
      header.serialize(test_bytes);

      bool allclose = true;
      for (unsigned idx=0; idx<sizeof(header); idx++)
      {
        if (test_bytes[idx] != bytes[idx]) {
          allclose = false;
          break;
        }
      }
      CHECK(allclose == true);
    }

  }


}



TEST_CASE (
  "Protocol can load and unload bytes",
  "[protocol][Protocol]"
)
{
  SECTION ("protocol::Message::ctor") {
    pyro4::protocol::Message protocol;
  }

  SECTION ("protocol::Message::from_header") {

  }

  SECTION ("protocol::Message::to_bytes") {

  }
}


TEST_CASE (
  "can read data from streams in protocol::Message",
  "[protocol][read]"
)
{
  pyro4::protocol::Message msg;
  BufferStream stream;

  std::vector<char> bytes;
  std::string file_path;

  for (auto const& pair : header_file_names) {
    file_path = current_dir + pair.first;

    test::util::load_binary_data<char>(file_path, bytes);

    stream = BufferStream(bytes);

    pyro4::protocol::read<BufferStream>(stream, msg);
  }
}

TEST_CASE (
  "can write data from messages into streams",
  "[protocol][write]"
)
{

}
