#ifndef __util_h
#define __util_h

#include <iostream>
#include <vector>
#include <tuple>

#include <arpa/inet.h>


namespace pyro4 {
  namespace util {
    namespace inplace {
      // net_to_host converts network endian values to host endian values

      template<typename T>
      void net_to_host () { }

      template<typename T>
      void net_to_host (T& arg) { }

      template<>
      void net_to_host<unsigned short> (unsigned short& arg) {
        arg = ntohs(arg);
      }

      template<>
      void net_to_host<short> (short& arg) {
        arg = (short) ntohs(arg);
      }

      template<>
      void net_to_host<unsigned> (unsigned& arg) {
        arg = ntohl(arg);
      }

      template<>
      void net_to_host<int> (int& arg) {
        arg = (int) ntohl(arg);
      }

      template<typename T, typename... Types>
      void net_to_host (T& arg, Types&... Fargs) {
        net_to_host(arg);
        net_to_host(Fargs...);
      }

      // host_to_net converts host endian values to network endian values
      template<typename T>
      void host_to_net () { }

      template<typename T>
      void host_to_net (T& arg) { }

      template<>
      void host_to_net<unsigned short> (unsigned short& arg) {
        arg = htons(arg);
      }

      template<>
      void host_to_net<short> (short& arg) {
        arg = (short) htons(arg);
      }

      template<>
      void host_to_net<unsigned> (unsigned& arg) {
        arg = htonl(arg);
      }

      template<>
      void host_to_net<int> (int& arg) {
        arg = (int) htonl(arg);
      }

      template<typename T, typename... Types>
      void host_to_net (T& arg, Types&... Fargs) {
        host_to_net(arg);
        host_to_net(Fargs...);
      }

    }

    namespace outplace {
      template<typename T>
      std::tuple<> host_to_net () { return std::make_tuple<>(); }

      template<typename T>
      std::tuple<T> host_to_net (T& arg);

      template<>
      std::tuple<unsigned short> host_to_net<unsigned short> (unsigned short& arg) {
        return std::make_tuple<unsigned short>(htons(arg));
      }

      template<>
      std::tuple<short> host_to_net<short> (short& arg) {
        return std::make_tuple<short>((short) htons(arg));
      }

      template<>
      std::tuple<unsigned> host_to_net<unsigned> (unsigned& arg) {
        return std::make_tuple<unsigned>(htonl(arg));
      }

      template<>
      std::tuple<int> host_to_net<int> (int& arg) {
        return std::make_tuple<int>((int) htonl(arg));
      }

      template<typename T, typename... Types>
      std::tuple<T, Types...> host_to_net (T& arg, Types&... Fargs) {
        return tuple_cat(
          host_to_net(arg),
          host_to_net(Fargs...)
        );
      }
    }
  }
}

#endif
