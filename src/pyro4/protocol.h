#ifndef __protocol_h
#define __protocol_h

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

namespace pyro4 {

  namespace protocol {

    extern int MSG_CONNECT;
    extern int MSG_CONNECTOK;
    extern int MSG_CONNECTFAIL;
    extern int MSG_INVOKE;
    extern int MSG_RESULT;
    extern int MSG_PING;
    extern int FLAGS_EXCEPTION;
    extern int FLAGS_COMPRESSED;
    extern int FLAGS_ONEWAY;
    extern int FLAGS_BATCH;
    extern int FLAGS_META_ON_CONNECT;
    extern int FLAGS_ITEMSTREAMRESULT;
    extern int FLAGS_KEEPSERIALIZED;

    extern int checksum_magic;

    class Message {

    public:

      // from Pyro4.message.Messsage.header_format: 4sHHHHiHHHH
      struct Header {
        char tag[4];
        unsigned short ver;
        unsigned short msg_type;
        unsigned short flags;
        unsigned short seq;
        int data_size;
        unsigned short serializer_id;
        unsigned short anns_size;
        unsigned short _reserved;
        unsigned short checksum;

        Header (
          const char* _tag,
          const unsigned short& _ver,
          const unsigned short& _msg_type,
          const unsigned short& _flags,
          const unsigned short& _seq,
          const int& _data_size,
          const unsigned short& _serializer_id,
          const unsigned short& _anns_size,
          const unsigned short& __reserved,
          const unsigned short& _checksum
        ): ver(_ver),
          msg_type(_msg_type),
          flags(_flags),
          seq(_seq),
          data_size(_data_size),
          serializer_id(_serializer_id),
          anns_size(_anns_size),
          _reserved(__reserved),
          checksum(_checksum)
        {
          for (unsigned idx=0; idx<sizeof(tag)/sizeof(tag[0]); idx++) {
            tag[idx] = _tag[idx];
          }
        }

        Header (const Header& _header);

        Header ();

        Header& operator=(const Header& _header);

        void serialize(std::vector<char>& bytes);

        static void deserialize(const std::vector<char>& bytes, Header& header);
      };

      unsigned short type;
      unsigned short flags;
      unsigned short seq;
      unsigned short serializer_id;
      std::string hmac_key;
      std::vector<char> data;
      std::vector<char> annotations;

      Message ();

      Message (
        const unsigned short& _type,
        const unsigned short& _flags,
        const unsigned short& _seq,
        const unsigned short& _serializer_id,
        const std::string& _hmac_key,
        char* _data,
        int _data_size,
        char* _annotations,
        int _annotations_size
      );

      void to_bytes (std::vector<char>& bytes) const;

      static void from_header (const std::vector<char>& bytes, Message& msg);

      void set_annotations_size (int _annotations_size);

      int get_annotations_size () const;

      void set_data_size (int _data_size);

      int get_data_size () const;

    protected:

      unsigned header_size = sizeof(Header);
      int data_size;
      int annotations_size;

      int calc_checksum () const;
    };

    // write a message to a stream
    template<class StreamType>
    void write (const StreamType& stream, const Message& msg);


    // read a message from a stream
    template<class StreamType>
    void read (const StreamType& stream, Message& msg);

  }
}

std::ostream& operator<<(std::ostream&, const pyro4::protocol::Message::Header&);

template<class StreamType>
void pyro4::protocol::write(
  const StreamType& stream,
  const pyro4::protocol::Message& msg
)
{
  std::vector<char> bytes;
  msg.to_bytes(bytes);
  stream.write(bytes.data(), bytes.size());
}


template<class StreamType>
void pyro4::protocol::read(
  const StreamType& stream,
  pyro4::protocol::Message& msg
)
{
  std::cerr << "pyro4::protocol::read" << std::endl;
  std::size_t header_size = sizeof(pyro4::protocol::Message::Header);

  std::vector<char> bytes(header_size);

  stream.read(bytes.data(), header_size);

  pyro4::protocol::Message::from_header (bytes, msg);

  std::cerr << "pyro4::protocol::read: getting annotations of size "
    << msg.get_annotations_size() << std::endl;
  // read annotations
  bytes.resize(msg.get_annotations_size());
  stream.read(bytes.data(), msg.get_annotations_size());
  std::copy(bytes.begin(), bytes.end(), msg.annotations.begin());


  std::cerr << "pyro4::protocol::read: getting data of size "
    << msg.get_data_size() << std::endl;
  // read data
  bytes.resize(msg.get_data_size());
  stream.read(bytes.data(), msg.get_data_size());
  std::copy(bytes.begin(), bytes.end(), msg.data.begin());

  std::cerr << "pyro4::protocol::read: done" << std::endl;

}




#endif
