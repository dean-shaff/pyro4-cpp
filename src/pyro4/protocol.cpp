#include <iostream>
#include <cstring>

#include "protocol.h"
#include "constants.h"
#include "util.h"

namespace pyro4 {
  namespace protocol {
    int MSG_CONNECT = 1;
    int MSG_CONNECTOK = 2;
    int MSG_CONNECTFAIL = 3;
    int MSG_INVOKE = 4;
    int MSG_RESULT = 5;
    int MSG_PING = 6;
    int FLAGS_EXCEPTION = 1 << 0;
    int FLAGS_COMPRESSED = 1 << 1;
    int FLAGS_ONEWAY = 1 << 2;
    int FLAGS_BATCH = 1 << 3;
    int FLAGS_META_ON_CONNECT = 1 << 4;
    int FLAGS_ITEMSTREAMRESULT = 1 << 5;
    int FLAGS_KEEPSERIALIZED = 1 << 6;

    int checksum_magic = 0x34E9;

    Message::Header::Header () {}

    Message::Header::Header (const Message::Header& _header) {
      std::cerr << "pyro4::protocol::Message::Header::Header "
        << "(const Message::Header&)" << std::endl;
      operator=(_header);
    }

    Message::Header& Message::Header::operator= (const Message::Header& _header) {
      if (&_header == this) {
        return *this;
      }
      for (unsigned idx=0; idx<sizeof(tag)/sizeof(tag[0]); idx++) {
        tag[idx] = _header.tag[idx];
      }
      ver = _header.ver;
      msg_type = _header.msg_type;
      flags = _header.flags;
      seq = _header.seq;
      data_size = _header.data_size;
      serializer_id = _header.serializer_id;
      anns_size = _header.anns_size;
      _reserved = _header._reserved;
      checksum = _header.checksum;
      return *this;
    }


    void Message::Header::serialize (
      std::vector<char>& bytes
    ) {
      std::cerr << "pyro4::protocol::Message::Header::serialize: ";
      Message::Header header(*this);

      util::inplace::host_to_net(
        header.ver, header.msg_type,
        header.flags, header.seq, header.data_size,
        header.serializer_id, header.anns_size,
        header._reserved, header.checksum
      );
      memcpy(bytes.data(), &header, sizeof(header));
    }

    void Message::Header::deserialize (
      const std::vector<char>& bytes,
      Message::Header& header
    ) {
      std::cerr << "pyro4::protocol::Message::Header::deserialize" << std::endl;
      const char* bytes_ptr = bytes.data();
      memcpy(&header, bytes_ptr, sizeof(header));
      util::inplace::net_to_host(
        header.ver, header.msg_type,
        header.flags, header.seq, header.data_size,
        header.serializer_id, header.anns_size,
        header._reserved, header.checksum);
    }



    Message::Message () {
    }

    Message::Message (
      const unsigned short& _type,
      const unsigned short& _flags,
      const unsigned short& _seq,
      const unsigned short& _serializer_id,
      const std::string& _hmac_key,
      char* _data,
      int _data_size,
      char* _annotations,
      int _annotations_size
    ): type(_type),
      flags(_flags),
      seq(_seq),
      serializer_id(_serializer_id),
      hmac_key(_hmac_key),
      data_size(_data_size),
      annotations_size(_annotations_size)
    {
      data.resize(data_size);
      annotations.resize(annotations_size);

      std::copy(
        _data, _data + data_size, data.begin());
      std::copy(
        _annotations,
        _annotations + annotations_size,
        annotations.begin());
    }

    void Message::to_bytes (std::vector<char>& bytes) const {
      std::cerr << "pyro4::protocol::Message::to_bytes: ";
      int checksum = calc_checksum();

      Message::Header header(
        "PYRO", pyro4::constants::PROTOCOL_VERSION,
        type, flags, seq, data_size, serializer_id,
        annotations_size, 0, checksum
      );
      header.serialize(bytes);
    }

    void Message::from_header (const std::vector<char>& bytes, Message& msg) {

      Message::Header header;
      Message::Header::deserialize(bytes, header);
      std::cerr << "pyro4::protocol::Message::from_header: header=" << header << std::endl;

      msg.type = header.msg_type;
      msg.flags = header.flags;
      msg.seq = header.seq;
      msg.serializer_id = header.serializer_id;

      msg.set_data_size(header.data_size);
      msg.set_annotations_size(header.anns_size);

      int checksum = msg.calc_checksum();

      // std::cerr << "pyro4::protocol::Message::from_header: header.checksum="
      //   << header.checksum << ", checksum=" << checksum << std::endl;

      if (checksum != header.checksum) {
        throw "pyro4::protocol::Message::from_bytes: checksum not equal to header checksum";
      }

      if (header.ver != pyro4::constants::PROTOCOL_VERSION) {
        throw "pyro4::protocol::Message::from_bytes: protocol versions incompatible";
      }
      std::string tag_str(header.tag, sizeof(header.tag)/sizeof(header.tag[0]));
      if (tag_str != "PYRO") {
        throw "pyro4::protocol::Message::from_bytes: tags are not equal";
      }
    }

    int Message::calc_checksum () const {
      return (type + pyro4::constants::PROTOCOL_VERSION +
        data_size + annotations_size +
        serializer_id + flags + seq + checksum_magic) & 0xffff;
    }

    void Message::set_annotations_size (int _annotations_size) {
      annotations_size = _annotations_size;
      annotations.resize(annotations_size);
    }

    int Message::get_annotations_size () const { return annotations_size; }

    void Message::set_data_size (int _data_size) {
      data_size = _data_size;
      data.resize(data_size);
    }

    int Message::get_data_size () const { return data_size; }

  }
}

std::ostream& operator<< (
  std::ostream& os,
  const pyro4::protocol::Message::Header& header
)
{
  for (unsigned idx=0; idx<sizeof(header.tag)/sizeof(header.tag[0]); idx++) {
    os << header.tag[idx];
  }
  os << "{" << header.tag << ", "
  << header.ver << ", "
  << header.msg_type << ", "
  << header.flags << ", "
  << header.seq << ", "
  << header.data_size << ", "
  << header.serializer_id << ", "
  << header.anns_size << ", "
  << header._reserved << ", "
  << header.checksum << "}";
  return os;
}
